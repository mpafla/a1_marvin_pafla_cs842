#Marvin Pafla

import torch
import pyro
import pyro.infer
from pyro.infer import MCMC, NUTS, HMC
import pyro.optim
import pyro.distributions as dist
import pyro.poutine as poutine
import matplotlib.pyplot as plt


#pyro.set_rng_seed(101)

#gpu or cpu
run_on_gpu = False
number_of_cpu_cores = 1

if run_on_gpu:
    torch.set_default_tensor_type(torch.cuda.FloatTensor)
    number_of_cpu_cores = 1
device = torch.device('cuda' if torch.cuda.is_available() and run_on_gpu else 'cpu')

#data
data_urn0 = {f"sample_urn_0_{i}": torch.tensor(0, dtype=torch.float64, device=device) for i in list(range(14))}
data_urn1 = {f"sample_urn_1_{i}": torch.tensor(1, dtype=torch.float64) for i in list(range(14))}
data_urn0_urn1 = {**data_urn0, **data_urn1}

#number of runs per model
number_of_samples = 1000
number_of_warm_up = 1000


def model(data):
    #general bias
    bias = pyro.sample("bias", dist.Uniform(0, 10))
    #red bias in proportion to bias
    red_bias = pyro.sample("red_bias", dist.Uniform(0, bias))
    #black bias is inverse to red bias
    black_bias = bias - red_bias
    #sample proportions of red balls between 0 and 1 for three urns with the help of a beta distribution
    proportions_red = [pyro.sample(f'proportion_red_{i}', dist.Beta(red_bias, black_bias)) for i in list(range(3))]
    urns = []
    #assumption that the same amount of urns is picked from each urn
    for i, proportion_red in enumerate(proportions_red):
        draws = []
        for j in list(range(len(data))):
            sample_str = f'sample_urn_{i}_{j}'
            #pick ball with color 1: red; 0: black
            sample = pyro.sample(sample_str, dist.Bernoulli(proportions_red[i]))
            draws.append(sample)
        urns.append(draws)
    return urns

def conditioned_model(model, data):
    return poutine.condition(model, data=data)(data)

#jit_compile does not work on my gpu
kernel = NUTS(conditioned_model, jit_compile=not(run_on_gpu))
def get_mcmc_model():
    return MCMC(kernel,
            num_samples=number_of_samples,
            warmup_steps=number_of_warm_up,
            num_chains=number_of_cpu_cores,
            mp_context="spawn")

#train three different models for the three different conditions (i.e., they all have different data to condition on)
mcmc_models = [get_mcmc_model() for _ in range(3)]
data_samples = [{}, data_urn0, data_urn0_urn1]
variable_names = ["bias", "proportion_red_2"]

#plots
fig, axes = plt.subplots(nrows=2, ncols=3)
for ax, row in zip(axes[:,0], variable_names):
    ax.set_ylabel(row, rotation=0, size='large')

#run each model with its data and get the plot for bias and red_bias_2
for i, (mcmc_model, data) in enumerate(zip(mcmc_models, data_samples)):
    mcmc_model.run(model, data)
    mcmc_model.summary(prob=0.9)
    variables = mcmc_model.get_samples(number_of_samples)

    for variable_name in variable_names:
        if variable_name == "bias":
            plt.subplot(2, 3, i + 1)
        else:
            plt.subplot(2, 3, i + 4)
        x, bins, p = plt.hist(variables[variable_name].tolist(), density=True, stacked=False, bins=10, edgecolor='black',
                              linewidth=1.2)
        axes = plt.gca()
        axes.set_ylim([0, 1])
        if i == 0:
            plt.ylabel(f'Probability for {variable_name}')
        plt.xlabel(f"Condition {i+1}")
        for item in p:
            item.set_height(item.get_height() / sum(x))

fig.tight_layout()
plt.show()
fig.savefig("results.pdf", bbox_inches='tight')





